<?php
/*
Plugin Name:  Sample Plugin
Plugin URI:   http://yourdomain.com
Description:  Simple sample plugin to wram up. 
Version:      1.0
Author:       HQ developer
Author URI:   http://yourdomain.com
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  sample-plugin
Domain Path:  /languages
*/

function sample_signature($content) {
    // Only do this when a single post is displayed
    if ( is_single() ) {
        // Message you want to display after the post
        $content .= '<p class="follow-us">Simple-plugin adds a signature on your post pages</p>';
    } 
    // Return the content
    return $content;
}
// Hook our function to WordPress the_content filter
add_filter('the_content', 'sample_signature'); 